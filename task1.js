const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
//const got = require("got");

const inputFile = "input2.json";
const outputFile = "output2.json";

let output = {};
console.log("loading input file", inputFile);
jsonfile.readFile(inputFile, function(err, input) {
          console.log("loaded input file content", input);
          output.document = input;
          output.emails = [];
          output.document.names.forEach(name => {
                    console.log("fetch each name in the input file", name);
                    let reverseName = name.split("").reverse().join("");
                    console.log("reverse names", reverseName);
                    let email = reverseName + randomstring.generate(5) + "@gmail.com";
                    console.log("create email from generated data", email);
                    output.emails.push(email);
          });
          console.log("generated emails", output.emails);
          console.log("saving output file formatted with 2 space indenting");
          jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
                 console.log("All done!");
          });
});
